# Quiz

Simple quiz application, based on the API of [Open Trivia Databse](https://opentdb.com/), [Vue.js](https://vuejs.org/) & [Vuetify](http://vuetifyjs.com/).

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
